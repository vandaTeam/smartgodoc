# Conceptos Claves

### BPM

Gestión de Procesos de Negocio BPM es una disciplina de gestión que integra la estrategia y los objetivos de una organización con las expectativas y necesidades de los clientes, centrándose en los procesos de extremo a extremo. Reúne estrategias, objetivos, cultura, estructuras organizacionales, roles, políticas, metodologías y herramientas de TI para:

* Analizar, diseñar, implementar, controlar y mejorar continuamente los procesos de extremo a extremo, y&#x20;
* Establecer la gobernanza de los procesos.&#x20;

Se centra en la prestación de mejoras operacionales, o, en un cambio a gran escala, la transformación. Este enfoque centrado en procesos para la gestión empresarial se apoya en herramientas automatizadas para ofrecer un entorno operativo que soporte cambios rápidos y mejora continua. BPM ofrece una visión de la actividad empresarial a través del uso de modelos de procesos con reglas de negocio y operaciones operativas claramente visibles.

### Maquina de Estado

Una máquina de estado es un método alternativo de crear un proceso de negocio. Una máquina de estado es adecuada para procesos relacionados con el cambio de estados en lugar de un flujo de control. Un estado define lo que un artefacto puede hacer en un momento determinado.

### Reglas de negocio

Las reglas de negocio complementan los procesos de negocio y las máquinas de estado. Por ejemplo, si existe una condición con una variable, puede utilizar una regla de negocio para cambiar el valor de esa variable en tiempo de ejecución.

Los reglas de negocio se utilizan en las situaciones de negocio cotidianas para tomar una decisión en un conjunto específico dado de circunstancias. Esta decisión puede requerir muchas reglas para cubrir todas las circunstancias. Las reglas de negocio dentro de un proceso de negocio permiten que las aplicaciones respondan rápidamente a condiciones de negocio cambiantes.

### Integración

La integracion de software es la practica de conectar y unificar diferentes tipos de partes o subsistemas de software. A menudo, las organizaciones pueden necesitar realizar la integracion de software porque estan realizando la transicion a una nueva aplicacion de datos basada en la nube desde un sistema heredado.

### API

Las API son mecanismos que permiten a dos componentes de software comunicarse entre sí mediante un conjunto de definiciones y protocolos. Por ejemplo, el sistema de software del instituto de meteorología contiene datos meteorológicos diarios. La aplicación meteorológica de su teléfono “habla” con este sistema a través de las API y le muestra las actualizaciones meteorológicas diarias en su teléfono.

### REST

REST significa transferencia de estado representacional. REST define un conjunto de funciones como GET, PUT, DELETE, etc. que los clientes pueden utilizar para acceder a los datos del servidor. Los clientes y los servidores intercambian datos mediante HTTP.

La principal característica de la API de REST es la ausencia de estado. La ausencia de estado significa que los servidores no guardan los datos del cliente entre las solicitudes. Las solicitudes de los clientes al servidor son similares a las URL que se escriben en el navegador para visitar un sitio web. La respuesta del servidor son datos simples, sin la típica representación gráfica de una página web.

### JSON

JSON (acrónimo de JavaScript Object Notation, 'notación de objeto de JavaScript') es un formato de texto sencillo para el intercambio de datos. Se trata de un subconjunto de la notación literal de objetos de JavaScript, aunque, debido a su amplia adopción como alternativa a XML, se considera un formato independiente del lenguaje.











