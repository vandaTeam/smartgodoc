# Roles Principales

El grado de influencia y los intereses de los participantes en los procesos pueden ser muy variados y diferentes. Los roles de participantes que describimos a continuación siempre están presentes de alguna forma en proyectos de BPM, pero en pocos casos nos encontramos con una organización orientada a procesos, como muestra la figura Nº1.

Se ha podido constatar que empresas que cuentan con mayores niveles de madurez en BPM también poseen roles bien definidos y estructuras orientadas a procesos. De todas formas recomendamos al lector que se está iniciando en BPM, familiarizarse con los roles de los participantes en la gestión orientada a procesos. He mantenido los términos en inglés porque las empresas globales actúan cada vez más en mercados internacionales. El equivalente en español lo encontrará entre los paréntesis (ver figura Nº2).

### **Process owner (dueño de proceso):**

&#x20;El process owner es el dueño de proceso y el responsable de plasmar la estrategia en los procesos. El debiera tener el mayor interés de todos los participantes en promover la mejora en la eficiencia de éstos. En muchas ocasiones las propuestas de mejora no vienen de él, sino de otras áreas y si lo logran convencer, él disponibiliza parte o gran parte del presupuesto para un proyecto de BPM. En la mayoría de las organizaciones el process owner es miembro de la alta gerencia o es responsable de un área o línea de negocio.

### Process manager (gestor de proceso):&#x20;

El process manager es el responsable de operaciones, reporta directamente al process owner y es él quien impulsa las propuestas de mejora. El es responsable de mantener la comunicación con los clientes y/o proveedores. Normalmente al process manager lo encontramos inserto en un nivel de jerarquía intermedia, como subdirector, subgerente, jefe de sucursal o jefe de grupo.

### Process participant (usuario, ejecutivo de negocio):&#x20;

Los process participants son los usuarios de negocio que trabajan en operaciones con el proceso, es decir, parte integrante de la cadena que crea valor para los clientes. Se pueden relacionar de muy diferentes maneras con el process manager. En la mayoría de las organizaciones son usuarios de un área funcional, como ventas, finanzas o logística. En estos casos no existe un process manager o actúa en su parte del proceso como tal y el usuario reporta directamente al encargado del área. Si la empresa está organizada en forma matricial, lo que en compañías globales es bastante común, pueden surgir conflictos entre el process manager y los responsables de áreas. En estructuras matriciales se requiere de un modelo de decisiones colaborativo para evitar el conflicto de intereses que surge en el punto de intersección.

### Process analyst (analista de proceso):&#x20;

Las competencias que se esperan del analista de procesos son conocimientos de BPM en general y de la notación de BPMN en específico (o bien de la notación escogida). El analista de procesos apoya al process manager como asesor interno o externo en todas las fases del ciclo de BPM. El puede representar, como experto, al process manager ante consultores externos o formar parte del equipo de proyectos de BPM. El analista de procesos puede ser miembro de un área de procesos de la empresa o pertenecer como analista al departamento de informática de ésta. En muy pocas ocasiones será el responsable de la implementación de los procesos, a pesar de que posee buenos conocimientos o una gran afinidad con las TI. El analista de procesoshit2.gif (7460 bytes) debiera de tener una gran habilidad en materias de desarrollo organizacional y técnicas de comunicación. Pero sobre todo es, como lo indica su rol, un analista. Se espera un gran dominio de la notación BPMN y, como coordinador entre personas de negocio y de TI, es un rol clave en cualquier proyecto de BPM. De acuerdo a nuestras observaciones y experiencias, más del 70% de las personas que ocupan este rol no cuentan con las competencias suficientes para cumplir con este objetivo. En la mayoría de los casos porque les faltan las habilidades para este perfil. La calificación más importante de un analista de procesos no es el comunicar, sino el captar o escuchar a los participantes. Buenos analistas de negocio sienten la necesidad de querer atender todo en detalle. Al mismo tiempo poseen la empatía, como para poder ponerse en el lugar del cliente y representar sus inquietudes. A ellos no se les escapa ningún detalle, pero al mismo tiempo poseen un buen sentido de abstracción y pueden reducir los modelos a su esencia. El perfil de un jefe de proyecto es diferente: está centrado en cumplir las metas del proyecto y, por lo general, prioriza las metas técnicas como fechas de entrega y mantención del presupuesto de costos de éste, ante aspectos de calidad y eficiencia. Por esta razón no se aconseja mezclar ambos roles en el sentido de que un jefe de proyectos actúe como analista o viceversa.

### Process engineer (ingeniero de proceso):&#x20;

El ingeniero de procesos implementa un modelo técnico a partir de la especificación y el diseño operacional validado por él y los analistas de procesos. El diseño técnico debe realizarse en el mismo entorno (process engine o BPMS) en donde se implementarán éstos. El ingeniero de procesos está bien capacitado en el entorno de implementación, configura y construye la solución de BPM en la suite escogida. El ingeniero de procesos también puede actuar como asesor en la fase de modelamiento de la lógica operacional.

### EAI developer (ingeniero de desarrollo y servicios):&#x20;

Un programador puede asumir el rol de ingeniero de desarrollo, si la solución requiere de ampliaciones o adaptaciones de desarrollo por medio de programación (servicios web, Java, C# u otros lenguajes).

En empresas y organizaciones de menor tamaño, muchos de estos participantes tendrán que asumir varios roles a la vez. Los siguientes roles en conjunto podrían por ejemplo asumir los participantes en las Pymes:

* Dueño de proceso y gestor de proceso
* Analista de negocio y ejecutivo de negocio
* Analista de negocio e ingeniero de procesos
* Arquitecto SOA e ingeniero de desarrollo.
