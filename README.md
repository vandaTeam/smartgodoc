---
description: Contenido y alcance del documento.
---

# SmartGO

SmartGO es un plataforma que permite controlar los procesos de negocios estableciendo responsables, plazos y eventos para lograr una trazabilidad completa de las etapas del proceso acompañada de toda la información necesaria para tener la mayor contenido a la hora de tomar decisiones.

Este documento tiene como objetivo entregar los conocimientos para que los equipos organizacionales puedan desarrollar flujos desde los más básico  en el diseñador hasta los mas complejos y personalizados con equipos de desarrollo internos.

La forma en que se aborda el contenido de este documento esta divido en perfiles de usuarios según áreas funcionales:

* **Diseñador**: es la persona que tiene la capacidad de modelar el flujo desde un alto nivel organizacional, capaz de tomar de entender y dirigir al negocio hacia el mejor modelo posible.

{% hint style="info" %}
Los conceptos claves para este perfil son BPM, procesos, integraciones. &#x20;
{% endhint %}

* **Desarrollador**: son las personas con conocimientos técnicos que tienen la capacidad de implementar las personalización necesarias para que el flujo pueda interactuar con los demás sistemas legados de la organización o bien realizar adaptaciones para complementar la interacción entre los flujos y los usuarios finales.

En la siguiente sección encontrara roles y funciones de un proceso de negocio.



&#x20;
