# Entorno

El entorno de ejecución de smartGO se divide en dos grandes componentes como se muestra en la siguiente figura:

![](<../.gitbook/assets/image (12).png>)

**SmartGO Core:** es el núcleo de ejecución principal de la plataforma donde se encuentra todos los eventos disponibles para ejecución en tiempo de real de los flujos.&#x20;

**SmartGO Bridge:** son procesos asíncronos que permiten la integración y generación de eventos con plataformas externas, también la generación de eventos de control para los flujos como eventos de recordatorio.
