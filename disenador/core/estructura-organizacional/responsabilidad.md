# Responsabilidad

Las responsabilidad son la relación que existe entre un rol y una [entidad](../entidades/), esta relación puede darse sobre una entidad de manera particular o de manera general a través de los tipos, clasificación, categoría o relaciones.

![](<../../../.gitbook/assets/image (1).png>)

Un ejemplo de responsabilidad es un supervisor que esta cargo de un cliente especifico, o bien de una marca en particular.

El [motor de responsabilidad](../flujos/responsables.md) es capaz de evaluar en tiempo de ejecución si alguna entidad esta asociada a algún rol y esta se asigna como responsable de manera automática.

{% hint style="info" %}
La responsabilidad dentro de la organización se esta trabajando en la próxima versión.
{% endhint %}





