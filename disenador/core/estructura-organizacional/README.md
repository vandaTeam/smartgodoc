# Estructura Organizacional

### Definición general

El modulo de estructura organizacional es el primer componente a definirse para iniciar cualquier tipo de operación dentro de SmartGO, como toda estructura la clave esta en definir jerárquicamente cada usuario de la aplicación de manera que el [Motor de Responsabilidad](../flujos/responsables.md) pueda asignar dinámicamente  a los responsables de cada flujo.

El componente principal de la estructura es el ROL que es la centro de todo el modelo como se aprecia en la siguiente figura:

![](<../../../.gitbook/assets/image (21).png>)



La descripción de estos componentes es la siguiente:

### Perfil

Es un grupo de roles que tienen en común las mismas responsabilidades, generalmente asociados a ramas mas altas de la estructura como por ejemplos supervisores, jefes, gerentes, etc.

Generalmente los perfiles son transversales a lugares físicos o arreas del negocio.

### Área

Son las divisiones en que se organiza cada empresa, cada área generalmente presenta su propia jerarquía como también puede ser dividida matricialmente.

### Edificio/Sucursal

Las sucursales son el lugar físico en donde se encuentra el rol idealmente que sea capaz de georeferenciarse. &#x20;

### Empresas

Las empresas están diseñadas para incluir responsabilidades que se encuentran fuera de la estructura organizacional pero tienen participación directa dentro de un flujo. por ejemplo empresas prestadoras de servicio, servicios transitorios, subcontratos, etc

{% hint style="info" %}
Por defecto se asume "Empresa Local".
{% endhint %}

### Usuario

Es la persona que ocupara el rol asignado, esta persona tiene acceso al sistema solo si se le asigna un rol.

{% hint style="danger" %}
El usuario no puede tener mas de un rol.
{% endhint %}



La estructura parte por defecto con un rol y usuario administrador que será el encargado de gestionar todo el negocio.

&#x20;





