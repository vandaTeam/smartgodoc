# Dependencia

Las dependencias son todos los roles que dependen directamente de otro, este es un vinculo fuerte que permite definir los ancestros y descendiente.

Una vez asignadas las dependencias el motor de responsabilidad es capaz de asumir los distintos niveles jerárquicos como padre, hijos.

![](<../../../.gitbook/assets/image (30).png>)

Las dependencias están directamente relacionadas con la organización y puede tener dependencias a roles que estén fuera de la empresa.
