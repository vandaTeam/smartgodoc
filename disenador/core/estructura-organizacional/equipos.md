# Equipos

{% hint style="warning" %}
En Construcción
{% endhint %}

Los equipos son agrupaciones transversales de roles que involucran a varias áreas, empresas, perfiles o sucursales.

![](<../../../.gitbook/assets/image (25).png>)

Los equipos estarán asociados a un rol coordinador y estos permitirán en el motor de responsabilidad asignar turnos, round robin, capacidades, etc.
