# Flujos

## Flujos

\
Flujos es el corazón de ejecución de SMARTGO, esta compuesto por un diseñador de maquina de estados donde cada estado representa una responsabilidad dentro de la organización con plazos y roles definidos para su resolución.

\
Como es una maquina de estados el usuario solo tiene dos opciones para diseñar el flujo que son:\


* Estado
* Cambio de estado

![](<../../../.gitbook/assets/image (22).png>)

{% hint style="info" %}
Existe una tercera opción que es un redirección a estado ya existentes.
{% endhint %}

### Estados

\
Los estados por definición son situaciones temporales que están a la espera de alguna acción que provoque un cambio en ellas. Los estados tienen un responsable definido, que debe tomar la decisión de sacar el estado de su situación actual y moverlo a un nuevo estado. Los cambios de estados están predefinidos por lo que tendrá q seleccionar entre las posibilidades que le presente el sistema.

![](<../../../.gitbook/assets/image (3).png>)



\
**Prorrogas:** existe la posibilidad de que el responsable pueda necesitar mas plazo para resolver subactividad y en ese caso debe aplicar una prorroga para q el estado no entre en alerta por plazo.\
Existe también la posibilidad de generar eventos transcurrido cierto tiempo el sistema pueda cambiar el estado.\
\
También puede darse que ciertos estados no tengan responsables y un BOT sea capaz de tomar decisiones de forma autónoma, estos Bot son programables por los desarrolladores del flujo.\


### Cambio de Estado

\
Cambio de estado: es todo evento que permite alterar el estado actual del flujo y llevarlo a uno nuevo. En esta acción pueden ocurrir varios escenarios.\
Que para avanzar se necesite información adicional.\
Una vez avanzado se disparan eventos que permitan comunicar a la organización o otros sistemas que se ha generado una acción.\
Para ello smartgo dispone de procesos predefinidos que facilitan



![](<../../../.gitbook/assets/image (6).png>)
