# Entidades

Es un modelo de datos extensible que busca representar de manera genérica cualquier interacción del negocio con el mundo exterior, generalmente las entidades están asociadas a clientes, productos, maquinarias, servicios, etc.

Para comprender el modelo de entidades es necesario tener en cuenta sus 3 niveles:

![](<../../../.gitbook/assets/image (8).png>)

### Tipo Entidad

El primer paso para definir una entidad es especificar su tipo, el tipo es un nombre genérico que permite a toda la organización identificarlo de manera clara, este nombre será el que agrupa de manera única todo el universo. Podríamos decir que es un meta-modelo que definirá el comportamiento del modelo entidad.

### Modelo Entidad

El modelo de entidades por defecto considera unos atributos que se consideran base, estos atributos son clasificación, categoría, estado.



![](<../../../.gitbook/assets/image (32).png>)

* **Clasificación:** es un segmentación arbitraria del negocio para agrupar entidades.&#x20;
* **Categoría:** es un segmentación arbitraria del negocio para agrupar entidades. Estado: define los posible estados en que se encuentre la instancia de la entidad.

{% hint style="info" %}
Estos atributos se pueden enlazar con la responsabilidad de los roles para definir asignaciones automáticas del motor de responsabilidad.
{% endhint %}

También se pueden generar escenarios diferentes por cada uno de los atributos.

Todos los atributos de las entidades tienen asociado un código que permite relacionar con algún sistema legado o bien utilizarlo para desarrollos personalizados.

![](<../../../.gitbook/assets/image (24).png>)

Existe ademas una sección de campos extensibles, que permite a los administradores del modelo personalizar. Estos campos pueden campos de textos, numéricos, fechas o grupos.

### Instancia

Una vez q se crea o instancia una entidad esta expande su alcance a nuevos contenidos que se permiten almacenar de forma de complementar la información de dicha entidad, estos contenidos son contactos, adjuntos, ubicación geográfica, fichas.

Los contactos de los usuarios pueden tener acceso a SmartGo portal.

![](<../../../.gitbook/assets/image (35).png>)
