# Table of contents

* [SmartGO](README.md)
  * [Primeros Pasos](smartgo/primeros-pasos.md)
  * [Conceptos Claves](smartgo/conceptos-claves.md)
  * [Roles Principales](smartgo/roles-principales.md)

## Diseñador

* [Entorno](disenador/entorno.md)
* [Core](disenador/core/README.md)
  * [Estructura Organizacional](disenador/core/estructura-organizacional/README.md)
    * [Dependencia](disenador/core/estructura-organizacional/dependencia.md)
    * [Responsabilidad](disenador/core/estructura-organizacional/responsabilidad.md)
    * [Equipos](disenador/core/estructura-organizacional/equipos.md)
  * [Entidades](disenador/core/entidades/README.md)
    * [Clasificacion y Categorias](disenador/core/entidades/clasificacion-y-categorias.md)
    * [Estados](disenador/core/entidades/estados.md)
    * [Contactos](disenador/core/entidades/contactos.md)
    * [Relaciones y Referencias](disenador/core/entidades/relaciones-y-referencias.md)
    * [Valores Definidos por el usuario](disenador/core/entidades/valores-definidos-por-el-usuario.md)
  * [Flujos](disenador/core/flujos/README.md)
    * [Estados](disenador/core/flujos/estados.md)
    * [Cambios de Estado](disenador/core/flujos/cambios-de-estado.md)
    * [Escenarios](disenador/core/flujos/escenarios.md)
    * [Responsables](disenador/core/flujos/responsables.md)
    * [Prorrogas](disenador/core/flujos/prorrogas.md)
    * [Valores](disenador/core/flujos/valores.md)
    * [Reglas](disenador/core/flujos/reglas.md)
    * [Operaciones](disenador/core/flujos/operaciones.md)
    * [Formularios](disenador/core/flujos/formularios.md)
    * [Email](disenador/core/flujos/email.md)
  * [Workflow](disenador/core/workflow/README.md)
    * [Instancia](disenador/core/workflow/instancia.md)
    * [Inbox](disenador/core/workflow/inbox.md)
    * [Vista de Estado](disenador/core/workflow/vista-de-estado.md)
    * [Extensiones](disenador/core/workflow/extensiones.md)

***

* [Bridge](bridge/README.md)
  * [MQ](bridge/mq.md)
  * [Generador de Eventos](bridge/generador-de-eventos.md)

## Desarrollo

* [Framework](desarrollo/framework.md)
* [Render UI](desarrollo/render-ui.md)
* [UI Front](desarrollo/ui-front.md)
* [BackEnd](desarrollo/backend.md)
* [Cola de Prosamiento](desarrollo/cola-de-prosamiento.md)
* [Modelos de datos](desarrollo/modelos-de-datos.md)
* [Monitor](desarrollo/monitor.md)
* [Plugins](desarrollo/plugins/README.md)
  * [Box](desarrollo/plugins/box.md)
  * [Plugins Inbox](desarrollo/plugins/plugins-inbox.md)
  * [Plugins de Estado](desarrollo/plugins/plugins-de-estado.md)
* [Extensiones](desarrollo/extensiones.md)
* [Ejemplos](desarrollo/ejemplos/README.md)
  * [Iniciar un Flujo](desarrollo/ejemplos/iniciar-un-flujo.md)
  * [Incluir valores, entidades, adjuntos](desarrollo/ejemplos/incluir-valores-entidades-adjuntos.md)
  * [Avanzar Estado](desarrollo/ejemplos/avanzar-estado.md)
  * [Utilizar Cola](desarrollo/ejemplos/utilizar-cola.md)
* [Ambiente Local](desarrollo/ambiente-local/README.md)
  * [Configuración del Bridge](desarrollo/ambiente-local/configuracion-del-bridge.md)

## Modulos de usuario

* [Inbox](modulos-de-usuario/inbox.md)
* [Entidades](modulos-de-usuario/entidades.md)
* [Reportes](modulos-de-usuario/reportes.md)
