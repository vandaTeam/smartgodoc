# Ejemplos

En este articulo veremos:

* [Como iniciar un Flujo](iniciar-un-flujo.md)
* [Iniciar un flujo con valores, entidades, adjuntos.](incluir-valores-entidades-adjuntos.md)
* Como avanzar un Flujo
* Utilizar la cola de procesamiento
