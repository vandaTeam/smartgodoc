# Configuración del Bridge

El bridge esta ubicado en la carpeta Monitor y para su ejecución basta con ejecutar la siguiente línea de comando.

```
perl cola.pl CORE
```

En Linux si desea dejar el servicio en ejecución solo basta con ejecutar el proceso con nohup o bien puede dejar el servicio en ejecución con la librería daemon [https://metacpan.org/pod/Proc::Daemon](https://metacpan.org/pod/Proc::Daemon) &#x20;

```
nohup perl cola.pl CORE 2> nh.error > nh.log & 
```

