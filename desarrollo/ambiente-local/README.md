# Ambiente Local

En esta sección revisaremos como montar un ambiente local de desarrollo para desarrollar extensiones de SmartGO.

los contenidos son los siguiente:

* Requerimientos de Software
* Pasos para descargar el código fuente.
* Configuración de la base de datos
* Servidor Virtual
* Debug

### Requerimientos de Software

Los requerimientos de software necesario para la implementación del ambiente son los siguientes:

* SmartGo Core
  * Apache web server
  * MariaDB TX Server  10.2.5 ó Mysql 8.0 Server
  * PHP 7.4
    * Librerías: curl, mysqli, openssl, gd2,intl
* SmartGO Bridge
  * Perl 5.7
  * Librerías: JSON, LWP::UserAgent, Try::Tiny

{% hint style="info" %}
Para acceder al repositorio de SmartGO debe contactarse con el proveedor para habilitar los desarrolladores.
{% endhint %}

### Pasos para descargar el código fuente

Los pasos a seguir para la instalación son los siguiente:

* Descargar el proyecto desde el repositorio git.&#x20;
  * [https://gitlab.com/vandaTeam/smartgov3.git](https://gitlab.com/vandaTeam/smartgov3.git)

```
git clone https://gitlab.com/vandaTeam/smartgov3.git
```

### Configuración de la base de datos

Para configurar debe tener instalado un servidor de base de datos ya sea en el equipo local o un servidor remoto.

{% hint style="info" %}
Para obtener información de la instalación del motor puede acceder a la pagina del proveedor:

[https://dev.mysql.com/doc/refman/8.0/en/](https://dev.mysql.com/doc/refman/8.0/en/)

[https://mariadb.com/docs/](https://mariadb.com/docs/)
{% endhint %}

Conectarse a través de un cliente:

```
mysql -u root -p
Enter password: ****
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 12686
Server version: 10.5.8-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

* Crear una base de datos para el proyecto.

```
create database sg_db;
```

* Seleccione la base de datos creada.

```
use sg_db;
```

* Ubicar el archivo db.sql en la raíz del sitio y ejecutar en la base de datos.

```
source /xampp/htdocs/smartgo/db.sg.sql;
```

* Configurar en el archivo Config.php ubicado en Class/Common/

Una vez instalado la aplicación debe acceder a su servidor web de desarrollo http://localhost e iniciar la aplicación con el usuario **admin** y pass **admin**

### **Servidor Virtual**

En caso que el servidor web tenga varias aplicaciones instalada debe configurar un servidor virtual en el archivo de configuración añadiendo al final del archivo las siguientes líneas.

```
// httpd.conf
Listen 8080 
<VirtualHost *:8080>
DocumentRoot "/xampp/htdocs/smartgo/" 
</Virtualhost>
```

{% hint style="info" %}
En este caso se utilizar el puerto 8080 pero el desarrollador podría seleccionar el puerto que el estime conveniente.
{% endhint %}

Una vez configurado el servidor virtual debe reiniciar el servidor web y acceder a la aplicación en http://localhost:8080

### Debug

si requiere hacer debug en el entorno de desarrollo recomendamos la librería xdebug para mayor información acceda a la web del proveedor [https://xdebug.org/](https://xdebug.org/)
